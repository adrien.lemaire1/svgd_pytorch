import torch
import numpy as np
import scipy.spatial.distance
import math
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from torch import optim


class SVGD():

    def __init__(self):
        pass


    def kernel(self, theta, theta2, h = -1):
        pairwise_dists = torch.cdist(theta,theta2)**2
        if h < 0: # if h < 0, using median trick
            h = torch.quantile(pairwise_dists,q=0.5).item()
            h = np.sqrt(0.5 * h / np.log(theta.shape[0]+1))

        # compute the rbf kernel
        if abs(h) <= 1e-8:
            h = 1
        pairwise_div = torch.div(pairwise_dists,(2*(h**2)))
        pairwise_neg = - pairwise_div
        Kij = torch.exp(pairwise_neg)
        return Kij


    def update(self, particles, lntarget, n_iter = 1, stepsize = 1e-4):
        # Check input
        if particles is None or lntarget is None:
            raise ValueError('particles or lnprob cannot be None!')
        theta = particles.clone().detach()
        theta.requires_grad_(True)
        sample_size = particles.size()[0]
        optimizer = optim.SGD([theta], lr=stepsize)
        for iter in range(n_iter):
            kij = self.kernel(theta,theta.detach())
            normali = torch.ones_like(kij).fill_diagonal_(0)
            kij.backward(normali)
            f = theta.grad.clone()
            theta.grad = None
            lnp = lntarget(theta)
            lnp.backward(torch.ones_like(lnp))
            gr = theta.grad.clone()
            theta.grad = None
            theta.grad = torch.add(torch.matmul(kij,gr),f)
            optimizer.step()
            optimizer.zero_grad()
            theta.requires_grad_(True)
        return theta

        def update_gradient_free(self, particles, lntarget, lnsurrogate, n_iter = 1, stepsize = 1e-5):
        if particles is None or lntarget is None:
            raise ValueError('particles or lnprob cannot be None!')
        theta = particles.clone().detach()
        theta.requires_grad_(True)
        optimizer = optim.SGD([theta], lr=stepsize)
        for iter in range(n_iter):
            kij = self.kernel(theta,theta.detach())
            normali = torch.ones_like(kij).fill_diagonal_(0)
            # normali = torch.ones_like(kij)
            kij.backward(normali)
            f = theta.grad.clone()
            theta.grad = None
            targ = lntarget(theta)
            lnp = lnsurrogate(theta, targ)
            lnp.backward(torch.ones_like(lnp))
            gr = theta.grad.clone()
            # theta.grad = None
            weight = torch.div(lnp,lntarget)
            theta.grad = torch.mul(weight,torch.add(torch.matmul(kij,gr),f))
            optimizer.step()
            optimizer.zero_grad()
            theta.requires_grad_(True)
        return theta

def DensiteNormale(x,mu,sigma):
    a = 1/(sigma * math.sqrt(2*math.pi))*math.exp(-0.5*((x-mu)/sigma)**2)
    return a

def test_func(x):
    return (1/3)*DensiteNormale(x,-2,1)+(2/3)*DensiteNormale(x,2,1)

def DensiteNormale3(x,mu,sigma):
    tmp1 = torch.sub(x,mu)
    tmp2 = torch.div(tmp1,sigma)
    tmp3 = tmp2.pow(2)
    tmp35 = torch.mul(tmp3,-0.5)
    tmp4 = torch.exp(tmp35)
    cons = 1/(sigma * math.sqrt(2*math.pi))
    result = torch.mul(tmp4,cons)
    return result

def test_func3(x):
    x1i = DensiteNormale3(x,-2,1)
    x2 = DensiteNormale3(x,2,1)
    x1 = torch.mul(x1i,1/3)
    interm_test_func = torch.add(x1,x2,alpha=2/3)
    result = torch.log(interm_test_func)
    return result

def Rosenbrock(x, a = 1, b = 100):
    tmp1 = torch.pow(x[:,0],2)
    tmp2 = torch.sub(x[:,1],tmp1)
    tmp3 = torch.pow(tmp2,2)
    tmp5 = - torch.sub(x[:,0],a)
    tmp6 = torch.pow(tmp5,2)
    res = torch.add(tmp6, tmp3, alpha = b)
    return res

def Sphere(x):
    xs = torch.pow(x[:,0],2)
    ys = torch.pow(x[:,1],2)
    res = torch.add(xs,ys)
    return res

def Himmelblau(x):
    xs = torch.pow(x[:,0],2)
    ys = torch.pow(x[:,1],2)
    x1 = torch.add(xs,x[:,1])
    x2 = torch.sub(x1,11)
    op1 = torch.pow(x2,2)
    y1 = torch.add(x[:,0],ys)
    y2 = torch.sub(y1,7)
    op2 = torch.pow(y2,2)
    res = torch.add(op1,op2)
    return res

def Easom(x):
    cx = torch.cos(torch.angle(x[:,0]))
    cy = torch.cos(torch.angle(x[:,1]))
    x2 = torch.sub(x[:,0], torch.pi)
    y2 = torch.sub(x[:,1], torch.pi)
    opx = torch.pow(x2,2)
    opy = torch.pow(y2,2)
    exp1 = - torch.add(opx,opy)
    exp2 = torch.exp(exp1)
    resp = - torch.mul(cx,cy)
    res = torch.mul(resp,exp2)
    return res

def Rosenbrock_forplot(x, y, a = 1, b = 100):
    return (a-x)**2+(b*(y-(x**2))**2)

def count_val(x, prec, target):
    r = x.clone()
    prec_tens = torch.tensor(np.array([prec,prec]*(r.size()[1]-1)))
    if torch.cuda.is_available():
        prec_tens = prec_tens.to("cuda")
    compteur = 0
    for d in r:
        # print(d)
        # print(prec_tens)
        if torch.abs(torch.sub(d,target)).sum() < prec_tens.sum():
            compteur += 1
    return compteur


if __name__ == '__main__':
    torch.autograd.set_detect_anomaly(True)
    shape = (10,2)
    g = torch.rand(shape)
    if torch.cuda.is_available():
        g = g.to("cuda")
    tmp = SVGD()
    #TEST ROSENBROCK
    compt = 0
    for i in range(30):
        print(i)
        g = torch.rand(shape)
        if torch.cuda.is_available():
            g = g.to("cuda")
        res = tmp.update(g,Rosenbrock)
        res2 = torch.round(res,decimals=1).cpu().detach().numpy()
        if torch.cuda.is_available():
            target = torch.tensor([1.,1.], device=torch.device('cuda'))
        res3 = count_val(res,0.1,target)
        compt += (res3/shape[0])
    print(compt/30)


    #Create Rosenbrock plot
    # fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    #
    # # Make data.
    # X1 = np.arange(-2, 2, 0.01)
    # Y1 = np.arange(-3, 3, 0.01)
    # X, Y = np.meshgrid(X1, Y1)
    # R = Rosenbrock_forplot(X,Y)
    #
    # # Plot the surface.
    # surf = ax.plot_surface(X, Y, R, cmap=cm.coolwarm,
    #                     linewidth=0, antialiased=False)
    #
    # # Customize the z axis.
    # ax.zaxis.set_major_locator(LinearLocator(10))
    # # A StrMethodFormatter is used automatically
    # ax.zaxis.set_major_formatter('{x:.02f}')
    #
    # # Add a color bar which maps values to colors.
    # fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()

    # TEST 1D
    # shape = (10,2)
    # g = torch.normal(-10,1,(100,1))
    # nb = np.round(g.numpy(),0)
    # # nb = g.numpy()
    # if torch.cuda.is_available():
    #     g = g.to("cuda")
    # tmp = SVGD()
    # res = tmp.update(g,test_func3)
    # # res2 = torch.round(res,decimals=2).cpu()
    # res2 = res.cpu()
    # X = np.linspace(-15,15,10000)
    # Y = [test_func(x) for x in X]
    # t = res2.detach().numpy()
    # u,v = np.unique(t, return_counts=True)
    # # u0,v0 = np.unique(nb, return_counts=True)
    # v = v / nb.size
    # # v0 = v0 / nb.size
    #
    # # plt.plot(u,v, color='red')
    # # print(t[0])
    # # ty = [test_func(x[0]) for x in t]
    # # plt.scatter(t,ty, marker='D')
    # plt.bar(u,v)
    # plt.plot(X,Y)
    # # plt.plot(u0,v0, color='lime')
    # plt.show()

